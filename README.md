# OT High Error Rate Identification

This script will help to identify OT modules producing specifically high error
rates as described in several logbook entries (e.g.
[here](https://lblogbook.cern.ch/OT/5354)).

It reads `.root` files from the presenter and extracts the OT error rates
per module. For each module, the number of runs is accumulated in which this
specific module has an error rate which es 5 standard deviations above the
mean error rate for the run.

## Running on lxplus

The script uses some non-standard python packages which need to be installed
via pip. Since the pip version on lxplus is deprecated, you need to install
pip for yourself (maybe there is a simpler way which I am not aware of...) via
```
wget https://bootstrap.pypa.io/get-pip.py
lb-run ROOT python get-pip.py --user
```
This will put pip into your `~/.local/bin/` directory. I am using `lb-run ROOT`
to use the same python version which which the `pyroot` we will later use has
been built.

After that you can install the python dependencies via (the first line might be
possible to skip)
```
python -m pip install --user setuptools
lb-run ROOT python -m pip install --user tqdm rootpy matplotlib numpy
```

Finally, since we need to run on `lxplus` to download above packages, you need
to copy the `.root`-files from the LHCb-gateway. The directory naming schema
should be ovbious:
```
scp lbgw:/hist/Savesets/2017/LHCb/OTDAQMon/09/30/*-EOR.root .
```

You might need to connect to lxplus via `ssh -Y` to forward X11. If that's the
case, you can run the script via
```
lb-run ROOT /afs/cern.ch/user/k/kheinick/public/high_error_modules/high_error_modules.py .
```

Alternatively the script can be downloaded at
https://gitlab.cern.ch/kheinick/LHCb-OT-high-error-modules.
